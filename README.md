# Recommending Jokes

A simple web collection of jokes which recommends users similar jokes based on their ratings and view preferences. Please see our glorious [Wiki](https://gitlab.com/Rallix/recommending-jokes/-/wikis/home) for more information.