namespace Jokes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Migration : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.AspNetUsers", new[] { "Voter_Id" });
            AddColumn("dbo.AspNetUsers", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.AspNetUsers", "Voter_Id", c => c.Int());
            CreateIndex("dbo.AspNetUsers", "Voter_Id");
        }
        
        public override void Down()
        {
            DropIndex("dbo.AspNetUsers", new[] { "Voter_Id" });
            AlterColumn("dbo.AspNetUsers", "Voter_Id", c => c.Int(nullable: false));
            DropColumn("dbo.AspNetUsers", "Discriminator");
            CreateIndex("dbo.AspNetUsers", "Voter_Id");
        }
    }
}
