namespace Jokes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DbGeneratedRatingId : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Ratings");
            AlterColumn("dbo.Ratings", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Ratings", "Id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.Ratings");
            AlterColumn("dbo.Ratings", "Id", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Ratings", "Id");
        }
    }
}
