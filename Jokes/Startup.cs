﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Jokes.Startup))]
namespace Jokes
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
