﻿using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Microsoft.AspNet.Identity.EntityFramework;
using Jokes.Models;

namespace Jokes.DAL
{
    public class JokesDbContext : IdentityDbContext<ApplicationUser>
    {

        public JokesDbContext() : base("DefaultConnection", throwIfV1Schema: false)
        {
            // force load of EntityFramework.SqlServer.dll into build
            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
            Database.SetInitializer<JokesDbContext>(new JokesInitializer());
        }

        public DbSet<Joke> Jokes { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DbSet<Voter> Voters { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingEntitySetNameConvention>();

            base.OnModelCreating(modelBuilder);
        }

        /// <summary>
        /// Ctor with db connection, required by data access layer tests
        /// </summary>
        /// <param name="connection">The database connection</param>
        public JokesDbContext(DbConnection connection) : base(connection, true)
        {
            Database.CreateIfNotExists();
        }

        public static JokesDbContext Create()
        {
            return new JokesDbContext();
        }
    }
}