using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using WebGrease.Css.Extensions;
using Jokes.Models;
using Jokes.Helpers;

namespace Jokes.DAL
{

    /// <summary> Initializes the data in the database whenever it is recreated. </summary>
    public class JokesInitializer : DropCreateDatabaseIfModelChanges<JokesDbContext>
            // TODO: DropCreateDatabaseAlways<JokesDbContext> when testing
    {

        public override void InitializeDatabase(JokesDbContext context)
        {
            base.InitializeDatabase(context);

            if (!context.Voters.Any())
            {
                // Only run when there are no voters yet
                // (otherwise IDs would overlap and be overwritten)
                Seed(context);
                context.SaveChanges();
            }
        }

        /// <summary> Repopulates the database with sample data. </summary>
        protected override void Seed(JokesDbContext context)
        {
            var jokes = LoadJokes(@"jester/jester_items.dat");
            jokes.OrderBy(joke => joke.Id).ForEach(joke => { context.Jokes.AddOrUpdate(joke); });
            context.SaveChanges();

            var (ratings, voters) = LoadRatings(@"jester/jester_ratings.dat", limit: 250);

            var votersChunks = voters.ToList().Split(1000);
            foreach (List<Voter> votersList in votersChunks)
            {                
                context.Voters.AddRange(votersList);
                context.SaveChanges();
            }

            // ID for rating is created automatically          
            var ratingsChunks = ratings.ToList().Split(1000);
            foreach (List<Rating> ratingsList in ratingsChunks)
            {                
                context.Ratings.AddRange(ratingsList);
                context.SaveChanges();
            }

            base.Seed(context);
        }

        /// <summary> Loads jokes from a given file. </summary>
        /// <param name="jokesFile">The path to the file with jokes in the App_Data folder.</param>
        /// <returns>A collection of jokes</returns>        
        IEnumerable<Joke> LoadJokes(string jokesFile)
        {
            string path = System.Web.Hosting.HostingEnvironment.MapPath($"~/App_Data/{jokesFile}");
            var jokes = new List<Joke>();
            if (string.IsNullOrWhiteSpace(path) || !File.Exists(path))
            {
                return jokes;
            }

            var regex = new Regex(@"(?<Id>\d+):\r\n(?<Content>.*?)(\r\n\r\n|$)",
                                  RegexOptions.ExplicitCapture | RegexOptions.Singleline);
            string text = File.ReadAllText(path);
            MatchCollection matches = regex.Matches(text);
            foreach (Match match in matches)
            {
                string content = match.Groups["Content"].Value;
                if (string.IsNullOrWhiteSpace(content)) continue;

                if (int.TryParse(match.Groups["Id"].Value, out int id))
                {
                    var joke = new Joke {Id = id, Content = content};
                    jokes.Add(joke);
                }
            }

            return jokes;
        }


        /// <summary> Loads the ratings from file and creates matching voter records.
        /// File must be ordered by VoterID.</summary>
        /// <param name="ratingsFile">The path to the file with ratings in the App_Data folder.</param>
        /// <param name="limit">Limit the number of individual voters loaded (to save time).</param>
        /// <returns>A tuple with a collection of ratings and voters.</returns>
        (IEnumerable<Rating> Ratings, IEnumerable<Voter> Voters) LoadRatings(string ratingsFile,
                                                                                     int limit = int.MaxValue)
        {
            string path = System.Web.Hosting.HostingEnvironment.MapPath($"~/App_Data/{ratingsFile}");
            var voters = new List<Voter>();
            var ratings = new List<Rating>();
            if (string.IsNullOrWhiteSpace(path) || !File.Exists(path))
            {
                return (ratings, voters);
            }

            var regex = new Regex(@"(?<VoterId>\d+)\s+(?<JokeId>\d+)\s+(?<Vote>[+-]?(\d+([.]\d*)?|[.]\d+))",
                                  RegexOptions.ExplicitCapture | RegexOptions.Compiled);

            IEnumerable<string> lines = File.ReadLines(path);
            int voterCount = 0;
            int lastId = -1;
            foreach (string line in lines)
            {
                Match match = regex.Match(line);
                if (!match.Success) continue;

                int voterId, jokeId;
                float vote;
                try
                {
                    voterId = int.Parse(match.Groups["VoterId"].Value);
                    jokeId = int.Parse(match.Groups["JokeId"].Value);
                    vote = float.Parse(match.Groups["Vote"].Value, CultureInfo.InvariantCulture);
                }
                catch (FormatException)
                {
                    continue; // discard wrongly formatted rating
                }

                // Loaded all ratings from some voter
                if (lastId != voterId)
                {
                    if (voterCount >= limit)
                    {
                        break; // loaded enough ratings already
                    }
                    lastId = voterId;
                    voters.Add(new Voter {Id = voterId});
                    voterCount++;
                }

                var rating = new Rating
                {
                        JokeId = jokeId,
                        VoterId = voterId,
                        Vote = vote
                };
                ratings.Add(rating);
            }

            return (ratings, voters);
        }

    }

}
