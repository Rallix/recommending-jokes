﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace Jokes.Models
{
    public class Joke
    {

        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Required]
        public int Id { get; set; }

        [Required] [AllowHtml]
        [DataType(DataType.MultilineText)]
        public string Content { get; set; }

        /// <summary> This joke's ratings by various users. </summary>
        public virtual ICollection<Rating> Ratings { get; set; }

        [NotMapped]
        public int CurrentRating { get; set; }

        [NotMapped]
        public string RecommendedBy { get; set; }
    }
}