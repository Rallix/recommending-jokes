﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Jokes.Models
{
    /// <summary> Represents the rating of a <see cref="Joke"/> by a <see cref="ApplicationUser"/>. </summary>
    public class Rating
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Range(-10.0, 10.0, ErrorMessage = "The rating of a joke must be between -10 and 10.")]
        public float? Vote { get; set; }

        public int VoterId { get; set; }
        public int JokeId { get; set; }

        public int RecommendedBy { get; set; }

        public virtual Voter Voter { get; set; }
        public virtual Joke Joke { get; set; }
    }
}